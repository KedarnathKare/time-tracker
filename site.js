//date function
$( function() {
    $( "#datepicker" ).datepicker();
  } );
$("#select-category").click(function(){
    $(".timing").show();
});
$("#start-btn").click(function(){
    $("#reset-btn").show();
});
$("#reset-btn").click(function(){
    var category = jQuery('#select-category').val();
    localStorage.category=jQuery('#timer-change').text();
    $("input[name='s']").val(0);
});

$("#datepicker").change(function(){
  var existingValues = localStorage.values;
  existingValues = $.parseJSON(existingValues);
  $.each(existingValues,function(key,value){
    var name = value['info'];
      var caterory_type = value['category'];
      var time_val = value['time'];
      var date = value['date'];
      if (date == jQuery("#datepicker").val()) {
        var markup = "<tr><td>" + name + "</td><td>" + caterory_type + "</td><td>" + time_val + "</td></tr>";
        $("table tbody").append(markup);
      }
  });
})
$("#reset-btn").click(function(){
    var name = $("#working-on").val();
    var caterory_type = $("#select-category").val();
    var time_val = localStorage.category;
    var markup = "<tr><td>" + name + "</td><td>" + caterory_type + "</td><td>" + time_val + "</td></tr>";
    $("table tbody").append(markup);

	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	var output = ((''+month).length<2 ? '0' : '') + month +  '/' +
	((''+day).length<2 ? '0' : '') + day +'/' +
	d.getFullYear();

    var arrStorage = {
		"category" : jQuery("#select-category").val(),
		'info' : jQuery("#working-on").val(),
		'time' : jQuery("#timer-change").text(),
		"date" : output,
	}

	var local = localStorage.values;
	if(local=="" || local==undefined){
		local = [];
	}
	else{
		local = $.parseJSON(local);
	}
	local.push(arrStorage);
	local = JSON.stringify(local);
	localStorage.values = local;
});
(function(){  
    $("#start-btn, #reset-btn").click(function(){
        switch($(this).html().toLowerCase())
        {
            case "start":
            $("#timer-change").timer({action: 'reset'});
                s = parseInt($("input[name='s']").val());
                if(isNaN(s))
                {
                    s = 0;
                    $("input[name='s']").val(0);
                }
                //you can specify action via object or a string
                $("#timer-change").timer({
                    action: 'start', 
                    seconds:s
                });
                $(this).html("Pause");
                $("input[name='s']").attr("disabled", "disabled");
                $("#timer-change").addClass("badge-important");
                break;
            
            case "resume":
                //you can specify action via string
                $("#timer-change").timer('resume');
                $(this).html("Pause")
                $("#timer-change").addClass("badge-important");
                break;
            
            case "pause":
                //you can specify action via object
                $("#timer-change").timer({action: 'pause'});
                $(this).html("Resume")
                $("#timer-change").removeClass("badge-important");
                break;
            case "stop":
            	
            	$('#start-btn').html("Start")
            	// $("#timer-change").timer({action: 'stop'});
            	$("#timer-change").timer({action: 'pause'});
            	// alert('this is alert');
            	break;
        }
    });
    
    $("#get-seconds-btn").click(function(){
        console.log($("#timer-change").timer("get_seconds"));
    });
})();